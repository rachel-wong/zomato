## :tomato: Zomato Restaurant App

![progressShot](/src/assets/progress1.png)

## :construction: Core functionalities

- ~~search list~~
- display results as input form is changed ("burg" should still reveal all results for "burger", "burgering", "burgers" or "hamburg")
- ~~results within Brisbane only~~
- ~~rating~~
- favourites
- pagination
- show separate review page for each restaurant on click

## :bomb: Bugs

- at the moment, the search function has to include a submit button so to avoid excessive api calls as the input form changes for every letter inputted. Need to find a way to use the "ENTER" keystroke as sufficient to trigger the submission

> `useEffect( () => { makeAPICall() }, [variable])`

- It seems that this hooks trigger set-up will actually create two API calls: one when the component is mounted on DOM, and when the variable in the dependency array changes. Need to find a way to use hooks that literally only runs when the dependency variable changes. Have referred to the original documentation [here](https://reactjs.org/docs/hooks-reference.html#useeffect).

-  cannot get to second level object (object within object) when using the data returned from the /restaurant zomato endpoint. So getting to user_rating.aggregate_rating always throws up problems with the server response gap.

## :gift: NPM packages
- used @fontawesome/react-fontawesome to generate stars for the ratings

## :star: Personal notes from learning

- this [stackoverflow](https://stackoverflow.com/questions/54302348/star-rating-component-logic-implementation-in-react) gave me some ideas on how to implement the star rating
- hiding the api key in `.env` must always be labelled `REACT_APP_WATEVERNAMEYOUWANT`
- images can only be relative import within the `/src` folder and not `/public`
- quotation marks are not needed for the api string when declared and assigned within `.env`