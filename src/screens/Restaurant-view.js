import React, {
  useState,
  useEffect
} from 'react'
// import axios from "axios"
import Loader from "../components/Loading"
import InfoBlock from "../components/InfoBlock"
import "../styles/restaurant-view.css"
import ReviewBlock from "../components/ReviewBlock"

const RestaurantScreen = ({
  match
}) => {

  // having issues setting data and preventing multiple calls
  const [loading, setLoading] = useState(false)
  const [eatery, setEatery] = useState("") // default empty object, nothing throw up error

  const getData = async () => {
    setLoading(false)
    const api = `https://developers.zomato.com/api/v2.1/restaurant?res_id=${match.params.id}`
    let data = await fetch(api, {
      headers : {
      "user-key": process.env.REACT_APP_ZOMATO_KEY // for chrome error
      }
    })
    let result = await data.json()
    setEatery(result)
    setLoading(true)
  }

  useEffect(() => {
    getData()
  }, [])
  
  console.log(eatery)
  return (
    <>
    {loading === false ? ( <div className="container"> <Loader/> </div>): (


    <div>
      <div className="restaurant_view-blocker"/>
      <div className="restaurant_view-restaurant">
        <div
          className={
            'restaurant_view-imageWrapper restaurant_view-imageWrapper--' +
            (!eatery.thumb ? 'default' : 'thumb')
          }
        >
          {eatery.thumb && (
            <div
              className="restaurant_view-image"
              style={{ backgroundImage: 'url(' + eatery.thumb + ')' }}
            />
          )}
        </div>
        <div className="restaurant_view-headerContent">
          {eatery.thumb && (
            <div className="restaurant_view-imageSmall">
              <img src={eatery.thumb} alt={eatery.name} />
            </div>
          )}
          <div>
            <h1 className="restaurant_view-name">{eatery.name}</h1>
            <div className="restaurant_view-address">{eatery.location.address}</div>
          </div>
        </div>
        <div className="restaurant_view-content">
          <div className="restaurant_view-info">
            <InfoBlock
              label="Average price for two"
              value={eatery.currency + eatery.average_cost_for_two}
            />
            <InfoBlock
              label="Online delivery"
              value={eatery.has_online_delivery ? ('Yes') : ('No')}
            />
              <InfoBlock label="Rating" value={`${eatery.user_rating.aggregate_rating} / 5`}/>
          </div>
          <div className="restaurant_view-info restaurant-reviews">
            {eatery.all_reviews ? 

            (
            
             eatery.all_reviews.reviews.map((item, idx) => (
            <ReviewBlock
            key={idx}
              maintext={item.review.review_text}
              user={item.review.user.name}
              timestamp = {item.review.timestamp}
            />
            ))
            ): ("No reviews available")}
            <div className="restaurant-view-info restaurant-review-totals"> 
            {eatery.all_reviews_count > 2 ? ("+" + (eatery.all_reviews_count - 2) + " reviews") : ("") }
            </div>
          </div>
        </div>
      </div>
    </div>
      // <div>
      //   <ul>
      //     <li>ID {eatery.id}</li>
      //     <li>Name: {eatery.name}</li>
      //     <li>Aggregate Rating: {eatery.user_rating ? (eatery.user_rating.aggregate_rating) : ("Loading rating")}</li>
      //     <li>Avg price for two: ${eatery.average_cost_for_two}</li>
      //     <li>{eatery.location ? (eatery.location.address) : ("No address yet")}</li>
      //     <li>Thumb URL: <img src={eatery.thumb} alt={eatery.name}/></li>
      //   </ul>
      //   <h3>Reviews</h3>
      //   <ul>
      //     {eatery.all_reviews ? (eatery.all_reviews.reviews.map((item, idx) => (
      //       <li key={idx}>{item.review.review_text}</li>
      //     ))) : (<p>Not reading yet</p>)}
      //     {eatery.all_reviews ? (eatery.all_reviews.reviews.map((item, idx) => (
      //       <li key={idx}>{item.review.rating} / 5</li>
      //     ))) : (<p>Not reading yet</p>)}
      //   </ul>
      //   </div>
      )}
    </>
    
  )
}

export default RestaurantScreen