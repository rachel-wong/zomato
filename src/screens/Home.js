import React, {
  useEffect,
  useState
} from 'react'
import axios from 'axios'
import Search from "../../src/components/SearchInput"
import Loading from "../../src/components/Loading"
import "../styles/home.css"
import Card from "../../src/components/Card"
import Pagination from "../../src/components/Pagination"

const API_KEY = process.env.REACT_APP_ZOMATO_KEY

const Home = () => {

  // create useStates to get searchInput, when search query is submitted, loading
  const [restaurants, setRestaurants] = useState(null) // grabs api call data
  const [searchTerm, setSearchTerm] = useState("") // grabs searchTerm
  const [query, setQuery] = useState("") // this is needed so to only trigger api call when form is submitted
  const [loader, setLoader] = useState(true)
  const [like, setLike] = useState(false)
  const [resPerPage] = useState(9)
  const [totalRes, setTotalRes] = useState(0) 
  const [currentPage, setCurrentPage] = useState(0) // default current page is first page. This is "results_shown" in the zomato api data. 

  const getData = async (currentPage) => {
    setLoader(true)
    const url = `https://developers.zomato.com/api/v2.1/search?entity_id=298&entity_type=city&q=${query}&lat=-27.470125&lon=153.021072&count=${resPerPage}&start=${currentPage}`
    
    const result = axios.get(url, {
        headers: {
          Accept: "application/json",
          'user-key': API_KEY
        }
      }).then(res => {
        // when you make more than one setState method call, you need curly braces
          setRestaurants(res.data.restaurants)
          setTotalRes(res.data.results_found)
          setCurrentPage(res.data.results_start)
        })
      setLoader(false)
  }

  useEffect(() => {
    setLoader(false)
    // set up methods for getting form data, get api call data
    getData()
  }, [query, currentPage, like]) // run when query is set when form is submitted

  // Grabs the search term when the search input field changes
  const getTerm = (event) => {
    setSearchTerm(event.target.value)
  }

  const getQuery = (event) => {
    event.preventDefault()
    setQuery(searchTerm)
    setSearchTerm("")
  }

  const setFavourite = (event) => {
    setLike(!like)
    if (like == true) {
      addFav()
    }
  }

  const goToPage = (pageNumber) => {
    setCurrentPage(pageNumber + 1)
    getData(currentPage)
  }

  const addFav = () => {
    // add id, name of item to localStorage
  }

  const deleteFav = () => {
    // find id and name of item and remove from localStorage
  }
  return ( 
  <div>
    <div className="app-wrapper">
      <h1 className="app-heading"> Food Finder [Hooks]</h1>
      <span className="app-byline"> Powered by <a href = "https://www.zomato.com/"> Zomato </a>
    </span>
    <Search value={searchTerm} getQuery={getQuery} getTerm={getTerm}/> 
    <div className="home-wrapper" >
      <div className = "home-restaurants" > {
        restaurants == null || loader ? ( <Loading/> ) : (restaurants.map((business, idx) => (<Card key = {idx} name = {business.restaurant.name}
          url = {business.restaurant.url}
          thumb = {business.restaurant.thumb}
          id = {business.restaurant.id}
          rating = {business.restaurant.user_rating.aggregate_rating}
          setFavourite = {setFavourite}
          like={like}
          ></Card>
        )))
      }
      </div>
      <Pagination totalResults={totalRes} resPerPage={resPerPage} currentPage={currentPage} goToPage={goToPage}/>
    </div>
    </div> 
  </div>
  )
}

export default Home