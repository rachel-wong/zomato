import React from 'react'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

library.add(faStar)

// Math.ceil because to round UP to the next whole number 3.5 needs to round up to 4 to account for the .5 grade.
const Rating = ({rating}) => {
  const totalStars = 5
  return(
    <>
      {[...Array(totalStars)].map((el, i) => (
        i < Math.ceil(rating) -1 ? (
        <FontAwesomeIcon key={i} style={{color: 'gold', marginLeft: "0.2rem"}} icon="star"/>
        ) : (
        <FontAwesomeIcon key={i} icon="star" style={{color: "lightGrey", marginLeft: "0.2rem"}}/>
        )
      ))}
    </>
  )
}

export default Rating