import React from 'react'

import "../../src/styles/search-input.css"

const SearchInput = ({getTerm, getQuery, value}) => {
  return (
      <div>
        <form onSubmit={getQuery} type="text">
          <input type="text" onChange={getTerm} value={value} className="search_input-input" placeholder="Enter search term ... " />
          {/* <button type="submit">Search</button> */}
        </form>
      </div>
  )
}

export default SearchInput