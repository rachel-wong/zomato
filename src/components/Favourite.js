import React from "react"
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "../styles/restaurant-card.css"

library.add(faHeart)

const Favourite = ({setFavourite, like}) => {

  return (
    <>
    <button className="favouriteBttn" onClick={setFavourite}>
      {like === true ? <FontAwesomeIcon style={{color: 'lightGrey'}} icon="heart" /> : <FontAwesomeIcon style={{color: 'lightGrey'}} icon="heart" />}
    </button>
    </>
  )
}

export default Favourite