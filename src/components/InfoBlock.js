import React from 'react';

import "../styles/info-block.css"

function InfoBlock ({ label, value }) {
  return (
    <div className="info_block-block">
      <div className="info_block-label">{label}</div>
      <div className="info_block-value">{value}</div>
    </div>
  );
}

export default InfoBlock;
