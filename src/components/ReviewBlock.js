import React from 'react';

import "../styles/review-block.css"

function ReviewBlock ({ maintext, user, timestamp }) {

  let convert = new Date (timestamp * 1000)

  let today = new Date()
  let thisMth = today.getMonth()
  let thisYr = today.getFullYear()

  let numMonths = (thisYr - convert.getFullYear()) * 12 + (thisMth - convert.getMonth()) - 1

  return (
    <div className="review_block-block">
      <div className="review_block-label">{numMonths > 1 ? (numMonths + " months ago") : (numMonths + " month ago")} - {user}</div>
      <div className="review_block-value">{maintext}</div>
    </div>
  )
}

export default ReviewBlock