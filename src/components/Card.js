import React from 'react'
import "../../src/styles/restaurant-card.css"
import {Link} from 'react-router-dom'
import Rating from "../components/Rating"
// import Favourite from "../components/Favourite"


import { library } from '@fortawesome/fontawesome-svg-core'
import { faHeart} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

library.add(faHeart)

const Card = ({name, thumb, id, rating, url, setFavourite, like}) => {
  return (
    <>
      <div className="restaurant_card-card">
        {thumb ? 
        (
        <img src={thumb} alt={name} className="restaurant_card-image" />
        ) : (
        <div className="restaurant_card-imageEmpty" /> 
        )}
          <div className="restaurant_card-caption">
            <div className="restaurant_card-name">{name}</div>
            <div className="restaurant_card-rating">
              <Rating rating={rating}></Rating>
            </div>
          </div>
          <div className="restaurant_card-link">
            <button className="favouriteBttn" onClick={setFavourite}>
              {like === true ? 
                <FontAwesomeIcon style={{color: 'red'}} icon="heart" /> : 
                <FontAwesomeIcon style={{color: 'lightGrey'}} icon="heart" />}
            </button>
              {/* <Favourite id={id} setFavourite={setFavourite} like={like}></Favourite> */}
              <p className="reviewLink"><Link to={`/restaurant/${id}`}>Reviews</Link></p>
          </div>
        </div>
      </>
  )
}

export default Card