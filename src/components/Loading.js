import React from 'react'
import Spinner from "../../src/assets/spinner.gif"

export default function Loading() {
  return ( 
    <div className="spinner">
      <img src={Spinner} alt="Fetching restaurants ... " style={{width: "100%", height: "100%"}}/>
    </div>
  )
}