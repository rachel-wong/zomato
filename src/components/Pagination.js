import React from 'react'

export const Pagination = ({currentPage, resPerPage, totalResults, goToPage}) => {

  let pageNumbers = []

  for(let i = 1; i < Math.ceil(totalResults / resPerPage); i++) {
    pageNumbers.push(i)
  }

  return (
    <div style={{display: "flex", justifyContent:"center", alignItems: "center"}}>
      <ul className="pagination">
      {pageNumbers.map(number => (
        <li key={number} className="page-item"><a href="#" className="page-link" onClick={()=> goToPage(number)}>{number}</a></li>
      ))}
      </ul>
    </div>
  )
}

export default Pagination