import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Home from "../src/screens/Home"
import RestaurantScreen from "./screens/Restaurant-view"

import "../src/styles/app.css"

function App() {
  return (
    <div className="App">
        <Router>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/restaurant/:id" exact component={RestaurantScreen}/>
          </Switch>
        </Router>
    </div>
  )
}

export default App;